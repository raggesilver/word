import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import { library } from '@fortawesome/fontawesome-svg-core';
import { faSave, faHome, faUndo, faRedo, faAngleDown, faPrint, faEllipsisH } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faSave, faHome, faUndo, faRedo, faAngleDown, faPrint, faEllipsisH);

const app = createApp(App);

app.component('v-icon', FontAwesomeIcon);

app.use(store).use(router).mount('#app');
