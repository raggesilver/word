const spacer = '1rem';

const rules = [
  {
    name: 'w',
    suffixes: [100, 75, 50, 25, 0, 'auto'],
    responsive: true,
    content: (suf) => {
      if (typeof suf === 'number') return `{ width: ${suf}% !important }`;
      else return `{ width: ${suf} !important }`;
    },
  },
  {
    name: 'h',
    suffixes: [100, 75, 50, 25, 0, 'auto'],
    responsive: true,
    content: (suf) => {
      if (typeof suf === 'number') return `{ height: ${suf}% !important }`;
      else return `{ height: ${suf} !important }`;
    },
  },
  {
    name: 'min-w',
    suffixes: [100, 75, 50, 25, 0],
    responsive: true,
    content: (suf) => {
      if (typeof suf === 'number') return `{ min-width: ${suf}% !important }`;
      else return `{ min-width: ${suf} !important }`;
    },
  },
  {
    name: 'min-h',
    suffixes: [100, 75, 50, 25, 0],
    responsive: true,
    content: (suf) => {
      if (typeof suf === 'number') return `{ min-height: ${suf}% !important }`;
      else return `{ min-height: ${suf} !important }`;
    },
  },
  {
    name: 'max-w',
    suffixes: [100, 75, 50, 25, 0],
    responsive: true,
    content: (suf) => {
      if (typeof suf === 'number') return `{ max-width: ${suf}% !important }`;
      else return `{ max-width: ${suf} !important }`;
    },
  },
  {
    name: 'max-h',
    suffixes: [100, 75, 50, 25, 0],
    responsive: true,
    content: (suf) => {
      if (typeof suf === 'number') return `{ max-height: ${suf}% !important }`;
      else return `{ max-height: ${suf} !important }`;
    },
  },
  {
    name: ['p', 'pl', 'ph'],
    suffixes: [0, 1, 2, 3, 4, 5, 'auto'],
    responsive: true,
    content: (suf) => {
      if (typeof suf === 'number') {
        return `{ padding-left: (${([0, 0.25, 0.5, 1, 1.5, 3])[suf]} * $spacer) !important }`;
      }
      else return `{ padding-left: ${suf} !important }`;
    },
  },
  {
    name: ['p', 'pr', 'ph'],
    suffixes: [0, 1, 2, 3, 4, 5, 'auto'],
    responsive: true,
    content: (suf) => {
      if (typeof suf === 'number') {
        return `{ padding-right: (${([0, 0.25, 0.5, 1, 1.5, 3])[suf]} * $spacer) !important }`;
      }
      else return `{ padding-right: ${suf} !important }`;
    },
  },
  {
    name: ['p', 'pt', 'pv'],
    suffixes: [0, 1, 2, 3, 4, 5, 'auto'],
    responsive: true,
    content: (suf) => {
      if (typeof suf === 'number') {
        return `{ padding-top: (${([0, 0.25, 0.5, 1, 1.5, 3])[suf]} * $spacer) !important }`;
      }
      else return `{ padding-top: ${suf} !important }`;
    },
  },
  {
    name: ['p', 'pb', 'pv'],
    suffixes: [0, 1, 2, 3, 4, 5, 'auto'],
    responsive: true,
    content: (suf) => {
      if (typeof suf === 'number') {
        return `{ padding-bottom: (${([0, 0.25, 0.5, 1, 1.5, 3])[suf]} * $spacer) !important }`;
      }
      else return `{ padding-bottom: ${suf} !important }`;
    },
  },
  {
    name: ['m', 'ml', 'mh'],
    suffixes: [0, 1, 2, 3, 4, 5, 'auto'],
    responsive: true,
    content: (suf) => {
      if (typeof suf === 'number') {
        return `{ margin-left: (${([0, 0.25, 0.5, 1, 1.5, 3])[suf]} * $spacer) !important }`;
      }
      else return `{ margin-left: ${suf} !important }`;
    },
  },
  {
    name: ['m', 'mr', 'mh'],
    suffixes: [0, 1, 2, 3, 4, 5, 'auto'],
    responsive: true,
    content: (suf) => {
      if (typeof suf === 'number') {
        return `{ margin-right: (${([0, 0.25, 0.5, 1, 1.5, 3])[suf]} * $spacer) !important }`;
      }
      else return `{ margin-right: ${suf} !important }`;
    },
  },
  {
    name: ['m', 'mt', 'mv'],
    suffixes: [0, 1, 2, 3, 4, 5, 'auto'],
    responsive: true,
    content: (suf) => {
      if (typeof suf === 'number') {
        return `{ margin-top: (${([0, 0.25, 0.5, 1, 1.5, 3])[suf]} * $spacer) !important }`;
      }
      else return `{ margin-top: ${suf} !important }`;
    },
  },
  {
    name: ['m', 'mb', 'mv'],
    suffixes: [0, 1, 2, 3, 4, 5, 'auto'],
    responsive: true,
    content: (suf) => {
      if (typeof suf === 'number') {
        return `{ margin-bottom: (${([0, 0.25, 0.5, 1, 1.5, 3])[suf]} * $spacer) !important }`;
      }
      else return `{ margin-bottom: ${suf} !important }`;
    },
  },
  {
    name: 'd',
    suffixes: [
      'flex', 'inline', 'inline-block', 'block', 'grid', 'table', 'table-cell', 'none',
    ],
    suffixFirst: false,
    responsive: true,
    content: (suf) => `{ display: ${suf} !important }`,
  },
  {
    name: 'col',
    suffixes: [null],
    responsive: true,
    content: () => {
      return '{ display: flex !important; flex-direction: column !important; }';
    },
  },
  {
    name: 'row',
    suffixes: [null],
    responsive: true,
    content: () => {
      return '{ display: flex !important; flex-direction: row !important; }';
    },
  },
  {
    name: 'rcol',
    suffixes: [null],
    responsive: true,
    content: () => {
      return '{ display: flex !important; flex-direction: column-reverse !important; }';
    },
  },
  {
    name: 'rrow',
    suffixes: [null],
    responsive: true,
    content: () => {
      return '{ display: flex !important; flex-direction: row-reverse !important; }';
    },
  },
  {
    name: 'jc',
    suffixes: ['start', 'center', 'end', 'stretch'],
    responsive: true,
    content: (suf) => {
      const f = (suf === 'start' || suf === 'end') ? 'flex-' : '';
      return `{ justify-content: ${f}${suf} !important; }`;
    },
  },
  {
    name: 'js',
    suffixes: ['start', 'center', 'end', 'stretch'],
    responsive: true,
    content: (suf) => {
      const f = (suf === 'start' || suf === 'end') ? 'flex-' : '';
      return `{ justify-self: ${f}${suf} !important; }`;
    },
  },
  {
    name: 'ai',
    suffixes: ['start', 'center', 'end', 'stretch'],
    responsive: true,
    content: (suf) => {
      const f = (suf === 'start' || suf === 'end') ? 'flex-' : '';
      return `{ align-items: ${f}${suf} !important; }`;
    },
  },
  {
    name: 'as',
    suffixes: ['start', 'center', 'end', 'stretch'],
    responsive: true,
    content: (suf) => {
      const f = (suf === 'start' || suf === 'end') ? 'flex-' : '';
      return `{ align-self: ${f}${suf} !important; }`;
    },
  },
  {
    name: 'display',
    suffixes: [1, 2, 3, 4, 5, 6],
    responsive: true,
    content: (suf) => {
      return `{ font-size: ${([0, 6, 5.5, 5, 4.5, 4, 3.5])[suf]}rem; line-height: 1.2; }`;
    },
  },
  {
    name: 'grid-cols',
    suffixes: [1, 2, 3, 4, 5],
    responsive: true,
    content: (suf) => {
      return `{ grid-template-columns: repeat(${suf}, 1fr) !important; }`;
    },
  },
  {
    name: 'grid-rows',
    suffixes: [1, 2, 3, 4, 5],
    responsive: true,
    content: (suf) => {
      return `{ grid-template-rows: repeat(${suf}, 1fr) !important; }`;
    },
  },
  {
    name: ['grid-gap', 'grid-h-gap'],
    suffixes: [0, 1, 2, 3, 4, 5],
    responsive: true,
    content: (suf) => {
      return `{ column-gap: (${([0, 0.25, 0.5, 1, 1.5, 3])[suf]} * $spacer) !important; }`;
    },
  },
  {
    name: ['grid-gap', 'grid-v-gap'],
    suffixes: [0, 1, 2, 3, 4, 5],
    responsive: true,
    content: (suf) => {
      return `{ row-gap: (${([0, 0.25, 0.5, 1, 1.5, 3])[suf]} * $spacer) !important; }`;
    },
  },
  {
    name: ['overflow-x'],
    suffixes: ['auto', 'hidden', 'scroll'],
    responsive: true,
    content: (suf) => {
      return `{ overflow-x: ${suf}; }`;
    },
  },
  {
    name: ['overflow-y'],
    suffixes: ['auto', 'hidden', 'scroll'],
    responsive: true,
    content: (suf) => {
      return `{ overflow-y: ${suf}; }`;
    },
  },
  {
    name: ['col-span'],
    suffixes: [1, 2, 3, 4, 5, 'max'],
    responsive: true,
    content: (suf) => {
      const vals = { 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, max: '1 / -1' };
      return `{ grid-column: span ${vals[suf]}; }`;
    },
  },
];

const queries = [
  { qr: '@media only screen and (min-width: 480px)', id: 'sm' },
  { qr: '@media only screen and (min-width: 768px)', id: 'md' },
  { qr: '@media only screen and (min-width: 992px)', id: 'lg' },
  { qr: '@media only screen and (min-width: 1200px)', id: 'xl' },
];

function getSelector (name, suf = null, qr = null, sufFirst = false) {
  const nn = (name instanceof Array) ? name : [name];
  let res = '';

  const arr = nn.map(el => {
    let n = `.${el}`;

    if (sufFirst) {
      n += `${suf !== null ? `-${suf}` : ''}${qr !== null ? `-${qr}` : ''}`;
    }
    else {
      n += `${qr !== null ? `-${qr}` : ''}${suf !== null ? `-${suf}` : ''}`;
    }

    return n;
  });

  res += arr.join(', ');

  return (res);
}

const N_SPACES = process.env.SPACES || 2;
let indent;

// Begin writing output ========================================================

console.log(`/**
 * This file is generated using @raggesilver's themer, do not modify.
 */\n`);

console.log(`$spacer: ${spacer};\n`);

for (const r of rules) {
  indent = 0;
  const ind = ' '.repeat(indent * N_SPACES);

  for (const s of r.suffixes) {
    const selector = getSelector(r.name, s, null, !!r.suffixFirst);
    console.log(`${ind}${selector} ${r.content(s)}`);
  }
  console.log('');
}

console.log('');

for (const q of queries) {
  indent = 0;

  console.log(`${q.qr} {`);
  for (const r of rules.filter(r => r.responsive)) {
    indent = 1;
    const ind = ' '.repeat(indent * N_SPACES);

    for (const s of r.suffixes) {
      const selector = getSelector(r.name, s, q.id, !!r.suffixFirst);
      console.log(`${ind}${selector} ${r.content(s)}`);
    }
    console.log('');
  }
  console.log(`} /* Finish ${q.id} query */\n`);
}
