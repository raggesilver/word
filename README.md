# Word Clone

Microsoft Word clone.

> There are currently **zero** features implemented. Nothing in this project
> is supposed to work.

![Preview](./preview.png)

## Why

I chose to "clone" Microsoft Word simply because as a Linux user I had no access
to Office 365 (apart from their online tools) and I always found MS Word to look
a lot better than Libre Office.

## Roadmap

- Finish base layout & styling for main window (the one where you write stuff)
- Finish layout for other parts of the app
  - Startup screen
  - Settings screen
  - Dialogs
- Start implementing WYSIWYG editor features
  - Basic editing
  - Basic styling (headers, titles, sub-titles, etc.)
  - Keyboard shortcuts
- Give up once I try doing pagination
- Never touch this project again

## Disclaimer

This project is in no way associated with Microsoft or any of it's products.
